package com.mofum.scope.common.model;

import java.util.List;

/**
 * 范围上下文
 *
 * @author yuyang@qxy37.com
 * @since 2019-03-23
 **/
public class ScopeContext<Context extends Object> {

    /**
     * 用户
     */
    private String username;

    /**
     * 权限
     */
    private String permission;

    /**
     * 角色
     */
    private String role;

    /**
     * 范围
     */
    List<Scope> scopes;

    /**
     * 上下文
     */
    private Context context;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPermission() {
        return permission;
    }

    public void setPermission(String permission) {
        this.permission = permission;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public List<Scope> getScopes() {
        return scopes;
    }

    public void setScopes(List<Scope> scopes) {
        this.scopes = scopes;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }
}

