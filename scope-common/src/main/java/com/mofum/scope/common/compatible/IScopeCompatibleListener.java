package com.mofum.scope.common.compatible;

import com.mofum.scope.common.model.Permission;

/**
 * @author yuyang@qxy37.com
 * @since 2019-06-14
 **/
public interface IScopeCompatibleListener {

    String compatible(String sql, String dbType, Permission permission, Object params);

}