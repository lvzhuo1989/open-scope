package com.mofum.scope.common.model;

import java.util.List;

/**
 * 权限模型
 *
 * @author yuyang@qxy37.com
 * @since 2019-03-19
 **/
public class Permission {

    /**
     * 用户ID
     */
    private String userId;

    /**
     * 列名
     */
    @Deprecated
    private String column;

    /**
     * 表别名
     */
    @Deprecated
    private String tableArias;

    /**
     * 范围
     */
    private List<Scope> scopeCollections;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getColumn() {
        return column;
    }

    public void setColumn(String column) {
        this.column = column;
    }

    public String getTableArias() {
        return tableArias;
    }

    public void setTableArias(String tableArias) {
        this.tableArias = tableArias;
    }

    public List<Scope> getScopeCollections() {
        return scopeCollections;
    }

    public void setScopeCollections(List<Scope> scopeCollections) {
        this.scopeCollections = scopeCollections;
    }

    @Override
    public String toString() {
        return "Permission{" +
                "userId='" + userId + '\'' +
                ", column='" + column + '\'' +
                ", tableArias='" + tableArias + '\'' +
                ", scopeCollections=" + scopeCollections +
                '}';
    }
}
