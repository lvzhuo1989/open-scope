package com.mofum.scope.common.model;

/**
 * 参数
 *
 * @author yuyang@qxy37.com
 * @since 2019-03-20
 **/
public class Parameter {

    /**
     * 索引
     */
    private int index;

    /**
     * 参数名
     */
    private String name;

    /**
     * 值
     */
    private Object value;

    public Parameter() {
    }

    public Parameter(int index, String name, Object value) {
        this.index = index;
        this.name = name;
        this.value = value;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }
}
