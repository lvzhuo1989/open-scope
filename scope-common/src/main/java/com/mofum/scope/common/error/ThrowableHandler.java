package com.mofum.scope.common.error;

/**
 * 异常处理器
 *
 * @author yuyang@qxy37.com
 * @since 2019-03-27
 **/
public interface ThrowableHandler {

    void handler(Throwable e) throws Throwable;

}