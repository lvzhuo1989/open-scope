package com.mofum.scope.common;

/**
 * 范围认证器
 *
 * @author yuyang@qxy37.com
 * @since 2019-03-20
 **/
public interface IScopeAuthenticator<Params extends Object, ServiceException extends Exception> {

    /**
     * 测试通过
     * @param params
     * @return
     * @throws ServiceException
     */
    boolean testAccess(Params params) throws ServiceException;

}
