package com.mofum.scope.spring.web.constant;

/**
 * @author yuyang@qxy37.com
 * @since 2019-03-20
 **/
public class Constant {

    public static final String SCOPE_COLUMN = "scopeColumn";

    public static final String SCOPE_SCOPES = "scopeCollections";

    public static final String SCOPE_SERVICE_COLUMN = "serviceIds";

    public static final ScopeConfig SCOPE_CONFIG = ScopeConfig.CONFIG;
}
