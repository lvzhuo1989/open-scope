package com.mofum.scope.annotation.controller.parser;

import com.mofum.scope.annotation.controller.QueryScope;
import com.mofum.scope.annotation.controller.ServiceColumn;
import com.mofum.scope.common.annotation.AnnotationParser;
import com.mofum.scope.common.annotation.metadata.controller.MQueryScope;
import com.mofum.scope.common.annotation.metadata.controller.MServiceColumn;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 查询范围注解解析器
 *
 * @author yuyang@qxy37.com
 * @since 2019-03-20
 **/
public class QueryScopeParser<T> implements AnnotationParser<Class<T>, MQueryScope> {

    public Map<Field, MQueryScope> parseField(Class<T> tClass) {

        Map<Field, MQueryScope> map = new HashMap<Field, MQueryScope>();

        Field[] fields = tClass.getDeclaredFields();

        for (Field field : fields) {

            if (field.isAnnotationPresent(QueryScope.class)) {

                QueryScope atColumn = field.getAnnotation(QueryScope.class);

                map.put(field, parse(atColumn));

            }

        }

        return map;
    }

    public Map<Type, MQueryScope> parseType(Class<T> tClass) {

        Map<Type, MQueryScope> map = new HashMap<Type, MQueryScope>();

        Class<?> clazz = tClass;

        if (clazz.isAnnotationPresent(QueryScope.class)) {

            QueryScope atColumn = clazz.getAnnotation(QueryScope.class);

            map.put(clazz, parse(atColumn));

        }

        return map;
    }

    public Map<Method, MQueryScope> parseMethod(Class<T> tClass) {

        Map<Method, MQueryScope> map = new HashMap<Method, MQueryScope>();

        Method[] methods = tClass.getDeclaredMethods();

        for (Method method : methods) {

            if (method.isAnnotationPresent(QueryScope.class)) {

                QueryScope atColumn = method.getAnnotation(QueryScope.class);

                map.put(method, parse(atColumn));

            }

        }

        return map;
    }

    public Map<Method, List<MQueryScope>> parseParams(Class<T> tClass) {

        Map<Method, List<MQueryScope>> map = new HashMap<Method, List<MQueryScope>>();

        Method[] methods = tClass.getDeclaredMethods();

        for (Method method : methods) {

            List<MQueryScope> list = new ArrayList<MQueryScope>();

            Annotation[][] parameterAnnotations = method.getParameterAnnotations();

            for (Annotation[] parameterAnnotation : parameterAnnotations) {
                for (Annotation annotation : parameterAnnotation) {
                    if (annotation instanceof QueryScope) {
                        QueryScope atColumn = (QueryScope) annotation;
                        list.add(parse(atColumn));
                    }
                }
            }

            map.put(method, list);
        }

        return map;
    }

    public MQueryScope parse(QueryScope queryScope) {
        MQueryScope mQueryScope = new MQueryScope();
        mQueryScope.setEnableAuthenticator(queryScope.enableAuthenticator());
        mQueryScope.setExtractors(queryScope.extractors());
        mQueryScope.setAuthenticators(queryScope.authenticators());
        mQueryScope.setEnableExtractor(queryScope.enableExtractor());
        mQueryScope.setEnableConverter(queryScope.enableConverter());
        mQueryScope.setConverters(queryScope.converts());
        mQueryScope.setServiceColumns(queryScope.serviceColumns());
        mQueryScope.setEnableCustomServiceColumns(queryScope.enableCustomServiceColumns());
        mQueryScope.setType(queryScope.type());

        ServiceColumn[] serviceColumns = queryScope.columns();
        String[] stringServiceColumns = queryScope.serviceColumns();
        String type = queryScope.type();

        Map<String, MServiceColumn> serviceColumnMap = new HashMap<>();

        if (stringServiceColumns != null) {
            for (int i = 0; i < stringServiceColumns.length; i++) {
                MServiceColumn serviceColumn = new MServiceColumn();
                serviceColumn.setName(stringServiceColumns[i]);
                serviceColumn.setType(type);
                serviceColumnMap.put(stringServiceColumns[i], serviceColumn);
            }
        }

        if (serviceColumns != null) {

            ServiceColumnParser serviceColumnParser = new ServiceColumnParser();
            for (int i = 0; i < serviceColumns.length; i++) {
                MServiceColumn mServiceColumn = serviceColumnParser.parse(serviceColumns[i]);
                serviceColumnMap.put(mServiceColumn.getName(), mServiceColumn);
            }
        }
        MServiceColumn[] res = new MServiceColumn[serviceColumnMap.size()];

        int i = 0;
        for (String key : serviceColumnMap.keySet()) {
            res[i] = serviceColumnMap.get(key);
            i++;
        }
        mQueryScope.setColumns(res);
        return mQueryScope;
    }

}
