package com.mofum.scope.annotation.service;

/**
 * 范围列
 *
 * @author yuyang@qxy37.com
 * @since 2019-03-20
 **/
public @interface ColumnScope {

    /**
     * 列名
     *
     * @return
     */
    String value() default "";

    /**
     * 列类型
     *
     * @return
     */
    String type() default "";

    /**
     * 表名称
     *
     * @return
     */
    String tableName() default "";

}
