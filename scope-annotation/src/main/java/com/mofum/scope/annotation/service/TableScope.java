package com.mofum.scope.annotation.service;

import java.lang.annotation.*;

/**
 * 表范围
 *
 * @author yuyang@qxy37.com
 * @since 2019-03-20
 **/
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface TableScope {

    /**
     * 表名
     *
     * @return
     */
    String value() default "";

    /**
     * 表别名
     *
     * @return
     */
    String alias() default "";

    /**
     * 列
     *
     * @return
     */
    ColumnScope[] columns() default {};

}
